console.log("Hello World");

const BACKEND_URL = "http://localhost:5050/books";

async function deleteData(id = "")
{
  const data = {id}

  const request = new Request(BACKEND_URL + '/' + id,
  {
    method: 'DELETE',
    headers: {'Accept' : 'application.json','Content-Type' : 'application/json'},
    body: JSON.stringify(data)
  })
  const response = await fetch(request)

  if(response.status === 400)
    console.log("ERROR: 400")
  else if(response.status === 500)
    console.log("Server-side error: 500")

  const result = await response.json();
  console.log("Record deleted!")
  console.log(result)

  fetchData();
}

async function fetchData()
{
  const request = new Request(BACKEND_URL, {method: 'GET'});
  const response = await fetch(request);
  const result = await response.json();

  let htmlCode = '';
  htmlCode += '<div class=row>';

  for(var i = 0; i < result.length; i++)
  {
    htmlCode += '<div class="col-md-4" align="center" padding-top: 30px>';
    htmlCode += '<img src="' + result[i].isbn +'.jpg" alt="not-found.jpg" width=200 height=300>';
    htmlCode += '<br>' + result[i].title;
    htmlCode += '<br>' + result[i].author;
    htmlCode += '<br>$' + result[i].price;
    htmlCode += '<br>' + result[i].isbn;
    htmlCode += '<button onclick="deleteData(\'' + result[i]._id + '\')">Delete</button>';
    htmlCode += '<br><button type="button" class="btn btn-secondary" onclick=putData(\"' + result[i]._id + '\")>Update</button></br>';
    htmlCode += '</div>';
    if((i+1) % 3 == 0)
    {
      htmlCode += '</div>';
      htmlCode += '<div class="row">';
    }
  }
  htmlCode += '</div>';
  document.getElementById('grid').innerHTML += htmlCode;
}

async function postData()
{
  const data = {title: 'Javascript Tutorial', author: 'Tom Q.', price: 100}
  data.title = document.getElementById('title').value;
  data.author = document.getElementById('author').value;
  data.price = document.getElementById('price').value;
  data.isbn = document.getElementById('isbn').value;

  console.log(data);
  const request = new Request(BACKEND_URL, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });

  const response = await fetch(request);
  if(response.status === 400)
    console.log("Failure! There was an error")
  else if (response.status === 500)
    console.log("Failure! Server error")
  const result = await response.json();
  console.log("Record inserted successfully");
  console.log(result);
}

async function putData(id = '') {

  const data = {title: '', author: '', price: 0 , isbn: 0}

  data.title = document.getElementById('title').value;
  data.author = document.getElementById('author').value;
  data.price = document.getElementById('price').value;
  data.isbn = document.getElementById('isbn').value;
  data._id = document.getElementById('_id').value;
  console.log(data);

   const request = new Request(BACKEND_URL +'/'+ id, {
     method : 'PUT',
     headers: {'Accept': 'application/json',
               'Content-Type': 'application/json'},
     body: JSON.stringify(data)
   });
}
